package com.bongo.pca.organization;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

import static javax.persistence.GenerationType.IDENTITY;

@Table(indexes = {
        @Index(name = "idx_organization_org_name", columnList = "org_name")
})
@Entity(name = "organization")
public class Organization implements Serializable {
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Getter
    @Setter
    private Long id;

    @Getter
    @Setter
    @Column(name="org_name", length=50, nullable=false, unique=true)
    private String name;

    @Getter
    @Setter
    @Column(name="payroll_sor", length=25, nullable=false)
    private String payrollSOR;
}
