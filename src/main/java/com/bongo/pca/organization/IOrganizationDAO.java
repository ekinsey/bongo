package com.bongo.pca.organization;

import org.springframework.stereotype.Repository;

@Repository
public interface IOrganizationDAO extends OrganizationRepository {

    Organization findByName(String name);
}
