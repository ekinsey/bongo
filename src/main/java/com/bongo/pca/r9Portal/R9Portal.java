package com.bongo.pca.r9Portal;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

import static javax.persistence.GenerationType.IDENTITY;

@Table(name = "r9_portal")
@Entity
public class R9Portal {
    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = IDENTITY)
    private Long id;

    @Lob
    @Column(name = "POD")
    private String pod;

    @Lob
    @Column(name = "ISI_CLIENT_ID")
    private String isiClientId;

    @Lob
    @Column(name = "CLIENT_NAME")
    private String clientName;

    @Lob
    @Column(name = "CLIENT_OID")
    private String clientOid;

    @Lob
    @Column(name = "`Enterprise Version`")
    private String enterpriseVersion;

    @Lob
    @Column(name = "Enterprise")
    private String enterprise;

    @Lob
    @Column(name = "`HR Anytime`")
    private String hRAnytime;

    @Column(name = "ACS")
    private Integer acs;

    @Lob
    @Column(name = "`ADP DataCloud`")
    private String aDPDataCloud;

    @Lob
    @Column(name = "`ADP DataCloud-Benchmarks`")
    private String aDPDataCloudBenchmarks;

    @Lob
    @Column(name = "`ADP Document Cloud`")
    private String aDPDocumentCloud;

    @Lob
    @Column(name = "`ADP Marketplace`")
    private String aDPMarketplace;

    @Lob
    @Column(name = "`ADP Marketplace Advertising`")
    private String aDPMarketplaceAdvertising;

    @Lob
    @Column(name = "`ADP Reporting`")
    private String aDPReporting;

    @Column(name = "ADP4ME")
    private Integer adp4me;

    @Lob
    @Column(name = "`Benefits Admin`")
    private String benefitsAdmin;

    @Lob
    @Column(name = "COS")
    private String cos;

    @Column(name = "`Content Management System`")
    private Integer contentManagementSystem;

    @Lob
    @Column(name = "`Data Bridge`")
    private String dataBridge;

    @Lob
    @Column(name = "`DocuMax-CDROM`")
    private String docuMaxCDROM;

    @Lob
    @Column(name = "`Employee Rates`")
    private String employeeRates;

    @Lob
    @Column(name = "Enhanced")
    private String enhanced;

    @Lob
    @Column(name = "`Enterprise eTIME`")
    private String enterpriseETIME;

    @Lob
    @Column(name = "`Event Configuration`")
    private String eventConfiguration;

    @Lob
    @Column(name = "FSA")
    private String fsa;

    @Lob
    @Column(name = "`File Import Status`")
    private String fileImportStatus;

    @Lob
    @Column(name = "GLI")
    private String gli;

    @Lob
    @Column(name = "`Gender for CA Pay Data Report`")
    private String genderForCAPayDataReport;

    @Lob
    @Column(name = "`HR Benefits Company Directory`")
    private String hRBenefitsCompanyDirectory;

    @Lob
    @Column(name = "`HR eXpert`")
    private String hREXpert;

    @Lob
    @Column(name = "HWSE")
    private String hwse;

    @Lob
    @Column(name = "`Health Welfare`")
    private String healthWelfare;

    @Lob
    @Column(name = "`I-Hub`")
    private String iHub;

    @Lob
    @Column(name = "IPay")
    private String iPay;

    @Column(name = "ISI")
    private Integer isi;

    @Lob
    @Column(name = "`Integrated HR Benefits`")
    private String integratedHRBenefits;

    @Lob
    @Column(name = "`Integrated Pay eXpert`")
    private String integratedPayEXpert;

    @Lob
    @Column(name = "`Integrated ezLaborManager`")
    private String integratedEzLaborManager;

    @Lob
    @Column(name = "`LGBTQ Self Identification`")
    private String lGBTQSelfIdentification;

    @Lob
    @Column(name = "`Leave Administration`")
    private String leaveAdministration;

    @Lob
    @Column(name = "`MA ASO`")
    private String maAso;

    @Lob
    @Column(name = "`MA Enterprise eTIME for Mobile`")
    private String mAEnterpriseETIMEForMobile;

    @Lob
    @Column(name = "`Manual Checks`")
    private String manualChecks;

    @Lob
    @Column(name = "`Mobile Solutions`")
    private String mobileSolutions;

    @Lob
    @Column(name = "`My ADP`")
    private String myADP;

    @Lob
    @Column(name = "`MyADP Field Permissions`")
    private String myADPFieldPermissions;

    @Lob
    @Column(name = "`New Hire Wizard`")
    private String newHireWizard;

    @Lob
    @Column(name = "`Off Cycle Pay`")
    private String offCyclePay;

    @Lob
    @Column(name = "`Participant Service Center`")
    private String participantServiceCenter;

    @Lob
    @Column(name = "`Pay Cycle Processing`")
    private String payCycleProcessing;

    @Lob
    @Column(name = "`Pay eXpert`")
    private String payEXpert;

    @Lob
    @Column(name = "`PayCard - Total Pay Card`")
    private String payCardTotalPayCard;

    @Lob
    @Column(name = "PayForce")
    private String payForce;

    @Lob
    @Column(name = "`Payroll Action Items`")
    private String payrollActionItems;

    @Lob
    @Column(name = "`Payroll Activity`")
    private String payrollActivity;

    @Lob
    @Column(name = "`Payroll Calendar (New)`")
    private String payrollCalendarNew;

    @Lob
    @Column(name = "`Payroll Preview`")
    private String payrollPreview;

    @Lob
    @Column(name = "`Payroll Setup`")
    private String payrollSetup;

    @Lob
    @Column(name = "`Payroll Work Center`")
    private String payrollWorkCenter;

    @Lob
    @Column(name = "`Portal Company Directory`")
    private String portalCompanyDirectory;

    @Lob
    @Column(name = "`Prior Quarter Adjustments`")
    private String priorQuarterAdjustments;

    @Lob
    @Column(name = "QuickView")
    private String quickView;

    @Lob
    @Column(name = "`Rapid Pay`")
    private String rapidPay;

    @Lob
    @Column(name = "`Retirement Services`")
    private String retirementServices;

    @Lob
    @Column(name = "`SS Deferral`")
    private String sSDeferral;

    @Lob
    @Column(name = "`Screening and Sel Services`")
    private String screeningAndSelServices;

    @Lob
    @Column(name = "`Spending and Consumer Health`")
    private String spendingAndConsumerHealth;

    @Lob
    @Column(name = "`Support Center`")
    private String supportCenter;

    @Lob
    @Column(name = "TPiNet")
    private String tPiNet;

    @Lob
    @Column(name = "`Tax Credit Services`")
    private String taxCreditServices;

    @Lob
    @Column(name = "`Total Rewards`")
    private String totalRewards;

    @Lob
    @Column(name = "`TotalPay iNet`")
    private String totalPayINet;

    @Lob
    @Column(name = "WAVES")
    private String waves;

    @Lob
    @Column(name = "`Welcome Center`")
    private String welcomeCenter;

    @Lob
    @Column(name = "eI9")
    private String eI9;

    @Lob
    @Column(name = "`iPay Admin`")
    private String iPayAdmin;

    @Lob
    @Column(name = "iReports")
    private String iReports;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIReports() {
        return iReports;
    }

    public void setIReports(String iReports) {
        this.iReports = iReports;
    }

    public String getIPayAdmin() {
        return iPayAdmin;
    }

    public void setIPayAdmin(String iPayAdmin) {
        this.iPayAdmin = iPayAdmin;
    }

    public String getEI9() {
        return eI9;
    }

    public void setEI9(String eI9) {
        this.eI9 = eI9;
    }

    public String getWelcomeCenter() {
        return welcomeCenter;
    }

    public void setWelcomeCenter(String welcomeCenter) {
        this.welcomeCenter = welcomeCenter;
    }

    public String getWaves() {
        return waves;
    }

    public void setWaves(String waves) {
        this.waves = waves;
    }

    public String getTotalPayINet() {
        return totalPayINet;
    }

    public void setTotalPayINet(String totalPayINet) {
        this.totalPayINet = totalPayINet;
    }

    public String getTotalRewards() {
        return totalRewards;
    }

    public void setTotalRewards(String totalRewards) {
        this.totalRewards = totalRewards;
    }

    public String getTaxCreditServices() {
        return taxCreditServices;
    }

    public void setTaxCreditServices(String taxCreditServices) {
        this.taxCreditServices = taxCreditServices;
    }

    public String getTPiNet() {
        return tPiNet;
    }

    public void setTPiNet(String tPiNet) {
        this.tPiNet = tPiNet;
    }

    public String getSupportCenter() {
        return supportCenter;
    }

    public void setSupportCenter(String supportCenter) {
        this.supportCenter = supportCenter;
    }

    public String getSpendingAndConsumerHealth() {
        return spendingAndConsumerHealth;
    }

    public void setSpendingAndConsumerHealth(String spendingAndConsumerHealth) {
        this.spendingAndConsumerHealth = spendingAndConsumerHealth;
    }

    public String getScreeningAndSelServices() {
        return screeningAndSelServices;
    }

    public void setScreeningAndSelServices(String screeningAndSelServices) {
        this.screeningAndSelServices = screeningAndSelServices;
    }

    public String getSSDeferral() {
        return sSDeferral;
    }

    public void setSSDeferral(String sSDeferral) {
        this.sSDeferral = sSDeferral;
    }

    public String getRetirementServices() {
        return retirementServices;
    }

    public void setRetirementServices(String retirementServices) {
        this.retirementServices = retirementServices;
    }

    public String getRapidPay() {
        return rapidPay;
    }

    public void setRapidPay(String rapidPay) {
        this.rapidPay = rapidPay;
    }

    public String getQuickView() {
        return quickView;
    }

    public void setQuickView(String quickView) {
        this.quickView = quickView;
    }

    public String getPriorQuarterAdjustments() {
        return priorQuarterAdjustments;
    }

    public void setPriorQuarterAdjustments(String priorQuarterAdjustments) {
        this.priorQuarterAdjustments = priorQuarterAdjustments;
    }

    public String getPortalCompanyDirectory() {
        return portalCompanyDirectory;
    }

    public void setPortalCompanyDirectory(String portalCompanyDirectory) {
        this.portalCompanyDirectory = portalCompanyDirectory;
    }

    public String getPayrollWorkCenter() {
        return payrollWorkCenter;
    }

    public void setPayrollWorkCenter(String payrollWorkCenter) {
        this.payrollWorkCenter = payrollWorkCenter;
    }

    public String getPayrollSetup() {
        return payrollSetup;
    }

    public void setPayrollSetup(String payrollSetup) {
        this.payrollSetup = payrollSetup;
    }

    public String getPayrollPreview() {
        return payrollPreview;
    }

    public void setPayrollPreview(String payrollPreview) {
        this.payrollPreview = payrollPreview;
    }

    public String getPayrollCalendarNew() {
        return payrollCalendarNew;
    }

    public void setPayrollCalendarNew(String payrollCalendarNew) {
        this.payrollCalendarNew = payrollCalendarNew;
    }

    public String getPayrollActivity() {
        return payrollActivity;
    }

    public void setPayrollActivity(String payrollActivity) {
        this.payrollActivity = payrollActivity;
    }

    public String getPayrollActionItems() {
        return payrollActionItems;
    }

    public void setPayrollActionItems(String payrollActionItems) {
        this.payrollActionItems = payrollActionItems;
    }

    public String getPayForce() {
        return payForce;
    }

    public void setPayForce(String payForce) {
        this.payForce = payForce;
    }

    public String getPayCardTotalPayCard() {
        return payCardTotalPayCard;
    }

    public void setPayCardTotalPayCard(String payCardTotalPayCard) {
        this.payCardTotalPayCard = payCardTotalPayCard;
    }

    public String getPayEXpert() {
        return payEXpert;
    }

    public void setPayEXpert(String payEXpert) {
        this.payEXpert = payEXpert;
    }

    public String getPayCycleProcessing() {
        return payCycleProcessing;
    }

    public void setPayCycleProcessing(String payCycleProcessing) {
        this.payCycleProcessing = payCycleProcessing;
    }

    public String getParticipantServiceCenter() {
        return participantServiceCenter;
    }

    public void setParticipantServiceCenter(String participantServiceCenter) {
        this.participantServiceCenter = participantServiceCenter;
    }

    public String getOffCyclePay() {
        return offCyclePay;
    }

    public void setOffCyclePay(String offCyclePay) {
        this.offCyclePay = offCyclePay;
    }

    public String getNewHireWizard() {
        return newHireWizard;
    }

    public void setNewHireWizard(String newHireWizard) {
        this.newHireWizard = newHireWizard;
    }

    public String getMyADPFieldPermissions() {
        return myADPFieldPermissions;
    }

    public void setMyADPFieldPermissions(String myADPFieldPermissions) {
        this.myADPFieldPermissions = myADPFieldPermissions;
    }

    public String getMyADP() {
        return myADP;
    }

    public void setMyADP(String myADP) {
        this.myADP = myADP;
    }

    public String getMobileSolutions() {
        return mobileSolutions;
    }

    public void setMobileSolutions(String mobileSolutions) {
        this.mobileSolutions = mobileSolutions;
    }

    public String getManualChecks() {
        return manualChecks;
    }

    public void setManualChecks(String manualChecks) {
        this.manualChecks = manualChecks;
    }

    public String getMAEnterpriseETIMEForMobile() {
        return mAEnterpriseETIMEForMobile;
    }

    public void setMAEnterpriseETIMEForMobile(String mAEnterpriseETIMEForMobile) {
        this.mAEnterpriseETIMEForMobile = mAEnterpriseETIMEForMobile;
    }

    public String getMaAso() {
        return maAso;
    }

    public void setMaAso(String maAso) {
        this.maAso = maAso;
    }

    public String getLeaveAdministration() {
        return leaveAdministration;
    }

    public void setLeaveAdministration(String leaveAdministration) {
        this.leaveAdministration = leaveAdministration;
    }

    public String getLGBTQSelfIdentification() {
        return lGBTQSelfIdentification;
    }

    public void setLGBTQSelfIdentification(String lGBTQSelfIdentification) {
        this.lGBTQSelfIdentification = lGBTQSelfIdentification;
    }

    public String getIntegratedEzLaborManager() {
        return integratedEzLaborManager;
    }

    public void setIntegratedEzLaborManager(String integratedEzLaborManager) {
        this.integratedEzLaborManager = integratedEzLaborManager;
    }

    public String getIntegratedPayEXpert() {
        return integratedPayEXpert;
    }

    public void setIntegratedPayEXpert(String integratedPayEXpert) {
        this.integratedPayEXpert = integratedPayEXpert;
    }

    public String getIntegratedHRBenefits() {
        return integratedHRBenefits;
    }

    public void setIntegratedHRBenefits(String integratedHRBenefits) {
        this.integratedHRBenefits = integratedHRBenefits;
    }

    public Integer getIsi() {
        return isi;
    }

    public void setIsi(Integer isi) {
        this.isi = isi;
    }

    public String getIPay() {
        return iPay;
    }

    public void setIPay(String iPay) {
        this.iPay = iPay;
    }

    public String getIHub() {
        return iHub;
    }

    public void setIHub(String iHub) {
        this.iHub = iHub;
    }

    public String getHealthWelfare() {
        return healthWelfare;
    }

    public void setHealthWelfare(String healthWelfare) {
        this.healthWelfare = healthWelfare;
    }

    public String getHwse() {
        return hwse;
    }

    public void setHwse(String hwse) {
        this.hwse = hwse;
    }

    public String getHREXpert() {
        return hREXpert;
    }

    public void setHREXpert(String hREXpert) {
        this.hREXpert = hREXpert;
    }

    public String getHRBenefitsCompanyDirectory() {
        return hRBenefitsCompanyDirectory;
    }

    public void setHRBenefitsCompanyDirectory(String hRBenefitsCompanyDirectory) {
        this.hRBenefitsCompanyDirectory = hRBenefitsCompanyDirectory;
    }

    public String getGenderForCAPayDataReport() {
        return genderForCAPayDataReport;
    }

    public void setGenderForCAPayDataReport(String genderForCAPayDataReport) {
        this.genderForCAPayDataReport = genderForCAPayDataReport;
    }

    public String getGli() {
        return gli;
    }

    public void setGli(String gli) {
        this.gli = gli;
    }

    public String getFileImportStatus() {
        return fileImportStatus;
    }

    public void setFileImportStatus(String fileImportStatus) {
        this.fileImportStatus = fileImportStatus;
    }

    public String getFsa() {
        return fsa;
    }

    public void setFsa(String fsa) {
        this.fsa = fsa;
    }

    public String getEventConfiguration() {
        return eventConfiguration;
    }

    public void setEventConfiguration(String eventConfiguration) {
        this.eventConfiguration = eventConfiguration;
    }

    public String getEnterpriseETIME() {
        return enterpriseETIME;
    }

    public void setEnterpriseETIME(String enterpriseETIME) {
        this.enterpriseETIME = enterpriseETIME;
    }

    public String getEnhanced() {
        return enhanced;
    }

    public void setEnhanced(String enhanced) {
        this.enhanced = enhanced;
    }

    public String getEmployeeRates() {
        return employeeRates;
    }

    public void setEmployeeRates(String employeeRates) {
        this.employeeRates = employeeRates;
    }

    public String getDocuMaxCDROM() {
        return docuMaxCDROM;
    }

    public void setDocuMaxCDROM(String docuMaxCDROM) {
        this.docuMaxCDROM = docuMaxCDROM;
    }

    public String getDataBridge() {
        return dataBridge;
    }

    public void setDataBridge(String dataBridge) {
        this.dataBridge = dataBridge;
    }

    public Integer getContentManagementSystem() {
        return contentManagementSystem;
    }

    public void setContentManagementSystem(Integer contentManagementSystem) {
        this.contentManagementSystem = contentManagementSystem;
    }

    public String getCos() {
        return cos;
    }

    public void setCos(String cos) {
        this.cos = cos;
    }

    public String getBenefitsAdmin() {
        return benefitsAdmin;
    }

    public void setBenefitsAdmin(String benefitsAdmin) {
        this.benefitsAdmin = benefitsAdmin;
    }

    public Integer getAdp4me() {
        return adp4me;
    }

    public void setAdp4me(Integer adp4me) {
        this.adp4me = adp4me;
    }

    public String getADPReporting() {
        return aDPReporting;
    }

    public void setADPReporting(String aDPReporting) {
        this.aDPReporting = aDPReporting;
    }

    public String getADPMarketplaceAdvertising() {
        return aDPMarketplaceAdvertising;
    }

    public void setADPMarketplaceAdvertising(String aDPMarketplaceAdvertising) {
        this.aDPMarketplaceAdvertising = aDPMarketplaceAdvertising;
    }

    public String getADPMarketplace() {
        return aDPMarketplace;
    }

    public void setADPMarketplace(String aDPMarketplace) {
        this.aDPMarketplace = aDPMarketplace;
    }

    public String getADPDocumentCloud() {
        return aDPDocumentCloud;
    }

    public void setADPDocumentCloud(String aDPDocumentCloud) {
        this.aDPDocumentCloud = aDPDocumentCloud;
    }

    public String getADPDataCloudBenchmarks() {
        return aDPDataCloudBenchmarks;
    }

    public void setADPDataCloudBenchmarks(String aDPDataCloudBenchmarks) {
        this.aDPDataCloudBenchmarks = aDPDataCloudBenchmarks;
    }

    public String getADPDataCloud() {
        return aDPDataCloud;
    }

    public void setADPDataCloud(String aDPDataCloud) {
        this.aDPDataCloud = aDPDataCloud;
    }

    public Integer getAcs() {
        return acs;
    }

    public void setAcs(Integer acs) {
        this.acs = acs;
    }

    public String getHRAnytime() {
        return hRAnytime;
    }

    public void setHRAnytime(String hRAnytime) {
        this.hRAnytime = hRAnytime;
    }

    public String getEnterprise() {
        return enterprise;
    }

    public void setEnterprise(String enterprise) {
        this.enterprise = enterprise;
    }

    public String getEnterpriseVersion() {
        return enterpriseVersion;
    }

    public void setEnterpriseVersion(String enterpriseVersion) {
        this.enterpriseVersion = enterpriseVersion;
    }

    public String getClientOid() {
        return clientOid;
    }

    public void setClientOid(String clientOid) {
        this.clientOid = clientOid;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public String getIsiClientId() {
        return isiClientId;
    }

    public void setIsiClientId(String isiClientId) {
        this.isiClientId = isiClientId;
    }

    public String getPod() {
        return pod;
    }

    public void setPod(String pod) {
        this.pod = pod;
    }


}
