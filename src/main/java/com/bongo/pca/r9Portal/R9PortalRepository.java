package com.bongo.pca.r9Portal;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface R9PortalRepository extends JpaRepository<R9Portal, Long> {
}
