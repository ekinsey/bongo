package com.bongo.pca.paasPortal;

import javax.persistence.*;

import static javax.persistence.GenerationType.IDENTITY;

@Table(name = "paas_portal")
@Entity
public class PaasPortal {

    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = IDENTITY)
    private Long id;

    @Lob
    @Column(name = "POD")
    private String pod;

    @Lob
    @Column(name = "ISI_CLIENT_ID")
    private String isiClientId;

    @Lob
    @Column(name = "CLIENT_NAME")
    private String clientName;

    @Lob
    @Column(name = "CLIENT_OID")
    private String clientOid;

    @Lob
    @Column(name = "`Off Cycle Payments`")
    private String offCyclePayments;

    @Lob
    @Column(name = "iReports")
    private String iReports;

    @Lob
    @Column(name = "`Mobile Solutions`")
    private String mobileSolutions;

    @Lob
    @Column(name = "QuickView")
    private String quickView;

    @Lob
    @Column(name = "`Rapid Pay`")
    private String rapidPay;

    @Lob
    @Column(name = "MyADP")
    private String myADP;

    @Lob
    @Column(name = "`Part Service Center Tools`")
    private String partServiceCenterTools;

    @Lob
    @Column(name = "`Retirement Services`")
    private String retirementServices;

    @Lob
    @Column(name = "`Enterprise eTIME`")
    private String enterpriseETIME;

    @Lob
    @Column(name = "`IPay Federated`")
    private String iPayFederated;

    @Lob
    @Column(name = "`IPay Non-federated`")
    private String iPayNonFederated;

    @Lob
    @Column(name = "FSA")
    private String fsa;

    @Lob
    @Column(name = "`ADP Marketplace`")
    private String aDPMarketplace;

    @Lob
    @Column(name = "`Event Notification`")
    private String eventNotification;

    @Lob
    @Column(name = "`ADP Marketplace Advertising`")
    private String aDPMarketplaceAdvertising;

    @Lob
    @Column(name = "Tokes")
    private String tokes;

    @Lob
    @Column(name = "`Tokes - Permissions Profiles`")
    private String tokesPermissionsProfiles;

    @Lob
    @Column(name = "`Tokes - Pool Group Sets`")
    private String tokesPoolGroupSets;

    @Lob
    @Column(name = "`Prior Quarter Adjustments`")
    private String priorQuarterAdjustments;

    @Lob
    @Column(name = "`Payroll Setup`")
    private String payrollSetup;

    @Lob
    @Column(name = "`MyADP Field Permissions`")
    private String myADPFieldPermissions;

    @Lob
    @Column(name = "`Payroll Activity`")
    private String payrollActivity;

    @Lob
    @Column(name = "`Local Tax Assistance`")
    private String localTaxAssistance;

    @Lob
    @Column(name = "`Payroll Calendar (New)`")
    private String payrollCalendarNew;

    @Lob
    @Column(name = "`File Import Status`")
    private String fileImportStatus;

    @Lob
    @Column(name = "`LGBTQ Self Identification`")
    private String lGBTQSelfIdentification;

    @Lob
    @Column(name = "`Total Rewards`")
    private String totalRewards;

    @Lob
    @Column(name = "`Workforce Manager`")
    private String workforceManager;

    @Lob
    @Column(name = "`Pay Cycle Processing`")
    private String payCycleProcessing;

    @Lob
    @Column(name = "`ADP DataCloud`")
    private String aDPDataCloud;

    @Lob
    @Column(name = "`ADP DataCloud-Benchmarks`")
    private String aDPDataCloudBenchmarks;

    @Lob
    @Column(name = "`ADP Document Cloud`")
    private String aDPDocumentCloud;

    @Lob
    @Column(name = "`ADP Reporting`")
    private String aDPReporting;

    @Lob
    @Column(name = "`ADP Enterprise COS`")
    private String aDPEnterpriseCOS;

    @Lob
    @Column(name = "ALINE")
    private String aline;

    @Lob
    @Column(name = "`DocuMax-CDROM`")
    private String docuMaxCDROM;

    @Lob
    @Column(name = "`Enterprise HR`")
    private String enterpriseHR;

    @Column(name = "EnterprisePortal")
    private Integer enterprisePortal;

    @Lob
    @Column(name = "`Enterprise Java Applet`")
    private String enterpriseJavaApplet;

    @Lob
    @Column(name = "`Employee Rates`")
    private String employeeRates;

    @Lob
    @Column(name = "`Health Welfare`")
    private String healthWelfare;

    @Lob
    @Column(name = "`HR Anytime`")
    private String hRAnytime;

    @Lob
    @Column(name = "`iPay Admin`")
    private String iPayAdmin;

    public String getIPayAdmin() {
        return iPayAdmin;
    }

    public void setIPayAdmin(String iPayAdmin) {
        this.iPayAdmin = iPayAdmin;
    }

    public String getHRAnytime() {
        return hRAnytime;
    }

    public void setHRAnytime(String hRAnytime) {
        this.hRAnytime = hRAnytime;
    }

    public String getHealthWelfare() {
        return healthWelfare;
    }

    public void setHealthWelfare(String healthWelfare) {
        this.healthWelfare = healthWelfare;
    }

    public String getEmployeeRates() {
        return employeeRates;
    }

    public void setEmployeeRates(String employeeRates) {
        this.employeeRates = employeeRates;
    }

    public String getEnterpriseJavaApplet() {
        return enterpriseJavaApplet;
    }

    public void setEnterpriseJavaApplet(String enterpriseJavaApplet) {
        this.enterpriseJavaApplet = enterpriseJavaApplet;
    }

    public Integer getEnterprisePortal() {
        return enterprisePortal;
    }

    public void setEnterprisePortal(Integer enterprisePortal) {
        this.enterprisePortal = enterprisePortal;
    }

    public String getEnterpriseHR() {
        return enterpriseHR;
    }

    public void setEnterpriseHR(String enterpriseHR) {
        this.enterpriseHR = enterpriseHR;
    }

    public String getDocuMaxCDROM() {
        return docuMaxCDROM;
    }

    public void setDocuMaxCDROM(String docuMaxCDROM) {
        this.docuMaxCDROM = docuMaxCDROM;
    }

    public String getAline() {
        return aline;
    }

    public void setAline(String aline) {
        this.aline = aline;
    }

    public String getADPEnterpriseCOS() {
        return aDPEnterpriseCOS;
    }

    public void setADPEnterpriseCOS(String aDPEnterpriseCOS) {
        this.aDPEnterpriseCOS = aDPEnterpriseCOS;
    }

    public String getADPReporting() {
        return aDPReporting;
    }

    public void setADPReporting(String aDPReporting) {
        this.aDPReporting = aDPReporting;
    }

    public String getADPDocumentCloud() {
        return aDPDocumentCloud;
    }

    public void setADPDocumentCloud(String aDPDocumentCloud) {
        this.aDPDocumentCloud = aDPDocumentCloud;
    }

    public String getADPDataCloudBenchmarks() {
        return aDPDataCloudBenchmarks;
    }

    public void setADPDataCloudBenchmarks(String aDPDataCloudBenchmarks) {
        this.aDPDataCloudBenchmarks = aDPDataCloudBenchmarks;
    }

    public String getADPDataCloud() {
        return aDPDataCloud;
    }

    public void setADPDataCloud(String aDPDataCloud) {
        this.aDPDataCloud = aDPDataCloud;
    }

    public String getPayCycleProcessing() {
        return payCycleProcessing;
    }

    public void setPayCycleProcessing(String payCycleProcessing) {
        this.payCycleProcessing = payCycleProcessing;
    }

    public String getWorkforceManager() {
        return workforceManager;
    }

    public void setWorkforceManager(String workforceManager) {
        this.workforceManager = workforceManager;
    }

    public String getTotalRewards() {
        return totalRewards;
    }

    public void setTotalRewards(String totalRewards) {
        this.totalRewards = totalRewards;
    }

    public String getLGBTQSelfIdentification() {
        return lGBTQSelfIdentification;
    }

    public void setLGBTQSelfIdentification(String lGBTQSelfIdentification) {
        this.lGBTQSelfIdentification = lGBTQSelfIdentification;
    }

    public String getFileImportStatus() {
        return fileImportStatus;
    }

    public void setFileImportStatus(String fileImportStatus) {
        this.fileImportStatus = fileImportStatus;
    }

    public String getPayrollCalendarNew() {
        return payrollCalendarNew;
    }

    public void setPayrollCalendarNew(String payrollCalendarNew) {
        this.payrollCalendarNew = payrollCalendarNew;
    }

    public String getLocalTaxAssistance() {
        return localTaxAssistance;
    }

    public void setLocalTaxAssistance(String localTaxAssistance) {
        this.localTaxAssistance = localTaxAssistance;
    }

    public String getPayrollActivity() {
        return payrollActivity;
    }

    public void setPayrollActivity(String payrollActivity) {
        this.payrollActivity = payrollActivity;
    }

    public String getMyADPFieldPermissions() {
        return myADPFieldPermissions;
    }

    public void setMyADPFieldPermissions(String myADPFieldPermissions) {
        this.myADPFieldPermissions = myADPFieldPermissions;
    }

    public String getPayrollSetup() {
        return payrollSetup;
    }

    public void setPayrollSetup(String payrollSetup) {
        this.payrollSetup = payrollSetup;
    }

    public String getPriorQuarterAdjustments() {
        return priorQuarterAdjustments;
    }

    public void setPriorQuarterAdjustments(String priorQuarterAdjustments) {
        this.priorQuarterAdjustments = priorQuarterAdjustments;
    }

    public String getTokesPoolGroupSets() {
        return tokesPoolGroupSets;
    }

    public void setTokesPoolGroupSets(String tokesPoolGroupSets) {
        this.tokesPoolGroupSets = tokesPoolGroupSets;
    }

    public String getTokesPermissionsProfiles() {
        return tokesPermissionsProfiles;
    }

    public void setTokesPermissionsProfiles(String tokesPermissionsProfiles) {
        this.tokesPermissionsProfiles = tokesPermissionsProfiles;
    }

    public String getTokes() {
        return tokes;
    }

    public void setTokes(String tokes) {
        this.tokes = tokes;
    }

    public String getADPMarketplaceAdvertising() {
        return aDPMarketplaceAdvertising;
    }

    public void setADPMarketplaceAdvertising(String aDPMarketplaceAdvertising) {
        this.aDPMarketplaceAdvertising = aDPMarketplaceAdvertising;
    }

    public String getEventNotification() {
        return eventNotification;
    }

    public void setEventNotification(String eventNotification) {
        this.eventNotification = eventNotification;
    }

    public String getADPMarketplace() {
        return aDPMarketplace;
    }

    public void setADPMarketplace(String aDPMarketplace) {
        this.aDPMarketplace = aDPMarketplace;
    }

    public String getFsa() {
        return fsa;
    }

    public void setFsa(String fsa) {
        this.fsa = fsa;
    }

    public String getIPayNonFederated() {
        return iPayNonFederated;
    }

    public void setIPayNonFederated(String iPayNonFederated) {
        this.iPayNonFederated = iPayNonFederated;
    }

    public String getIPayFederated() {
        return iPayFederated;
    }

    public void setIPayFederated(String iPayFederated) {
        this.iPayFederated = iPayFederated;
    }

    public String getEnterpriseETIME() {
        return enterpriseETIME;
    }

    public void setEnterpriseETIME(String enterpriseETIME) {
        this.enterpriseETIME = enterpriseETIME;
    }

    public String getRetirementServices() {
        return retirementServices;
    }

    public void setRetirementServices(String retirementServices) {
        this.retirementServices = retirementServices;
    }

    public String getPartServiceCenterTools() {
        return partServiceCenterTools;
    }

    public void setPartServiceCenterTools(String partServiceCenterTools) {
        this.partServiceCenterTools = partServiceCenterTools;
    }

    public String getMyADP() {
        return myADP;
    }

    public void setMyADP(String myADP) {
        this.myADP = myADP;
    }

    public String getRapidPay() {
        return rapidPay;
    }

    public void setRapidPay(String rapidPay) {
        this.rapidPay = rapidPay;
    }

    public String getQuickView() {
        return quickView;
    }

    public void setQuickView(String quickView) {
        this.quickView = quickView;
    }

    public String getMobileSolutions() {
        return mobileSolutions;
    }

    public void setMobileSolutions(String mobileSolutions) {
        this.mobileSolutions = mobileSolutions;
    }

    public String getIReports() {
        return iReports;
    }

    public void setIReports(String iReports) {
        this.iReports = iReports;
    }

    public String getOffCyclePayments() {
        return offCyclePayments;
    }

    public void setOffCyclePayments(String offCyclePayments) {
        this.offCyclePayments = offCyclePayments;
    }

    public String getClientOid() {
        return clientOid;
    }

    public void setClientOid(String clientOid) {
        this.clientOid = clientOid;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public String getIsiClientId() {
        return isiClientId;
    }

    public void setIsiClientId(String isiClientId) {
        this.isiClientId = isiClientId;
    }

    public String getPod() {
        return pod;
    }

    public void setPod(String pod) {
        this.pod = pod;
    }
}
