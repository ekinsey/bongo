package com.bongo.pca.r9Portal;


import com.bongo.pca.organization.Organization;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(SpringExtension.class)
@DataJpaTest
class R9PortalRepositoryTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private R9PortalRepository r9PortalRepository;

    @BeforeEach
    void setUp(){
        R9Portal r9Portal = new R9Portal();
        r9Portal.setClientOid("G30XDNSAC2583ARG");
        r9Portal.setClientName("AME");
        entityManager.persistAndFlush(r9Portal);
    }

    @Test
    void findAll() {
        ArrayList<R9Portal> portalClients = (ArrayList<R9Portal>) r9PortalRepository.findAll();
        assertThat(portalClients.size()).isGreaterThan(0);
    }
}
