package com.bongo.pca.organization;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;

import static org.assertj.core.api.Assertions.assertThat;


@ExtendWith(SpringExtension.class)
@DataJpaTest
//@AutoConfigureTestDatabase
class OrganizationRepositoryTest {
    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private IOrganizationDAO organizationRepository;

    @BeforeEach
    void setUp() {
        Organization organization = new Organization();
        organization.setName("ACME");
        organization.setPayrollSOR("AutoPay");
        entityManager.persistAndFlush(organization);
    }

    @Test
    void findAll() {
        ArrayList<Organization> organizations = (ArrayList<Organization>) organizationRepository.findAll();
        assertThat(organizations.size()).isGreaterThan(0);
    }

    @Test
    void findName() {
        Organization organization = organizationRepository.findByName("ACME");
        assertThat(organization).isNotNull();
    }


}
